package manishkpr.mapprojecteroad.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import manishkpr.mapprojecteroad.R;

public class MapInfoAdapter implements GoogleMap.InfoWindowAdapter {

    TextView textLatitude, textLongitude, textLocalTime, textUtcTime,textTimezone;

    private String latitude,longitude,localTime,utcTime,timeZone;



    private Context context;
    private View view;

    public MapInfoAdapter(Context context) {
        this.context = context;
    }

    public View getInfoWindow(Marker marker) {
        return null;
    }

    public View getInfoContents(Marker marker) {

        if (view == null) {
            view          = LayoutInflater.from(context).inflate(R.layout.layout_google_map_window, null);
            textLatitude  = (TextView) view.findViewById(R.id.latitude);
            textLongitude = (TextView) view.findViewById(R.id.longitude);
            textLocalTime = (TextView) view.findViewById(R.id.localTime);
            textUtcTime   = (TextView) view.findViewById(R.id.utcTime);
            textTimezone  = (TextView) view.findViewById(R.id.timeZone);
        }

        textLatitude.setText(getLatitude());
        textLongitude.setText(getLongitude());
        textLocalTime.setText(getLocalTime());
        textUtcTime.setText(getUtcTime());
        textTimezone.setText(getTimeZone());
        return view;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getUtcTime() {
        return utcTime;
    }

    public void setUtcTime(String utctime) {
        this.utcTime = utctime;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
}

