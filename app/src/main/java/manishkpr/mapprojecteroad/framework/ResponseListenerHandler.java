package manishkpr.mapprojecteroad.framework;

import android.os.Handler;
import android.os.Message;

import manishkpr.mapprojecteroad.config.AppConfig;


public class ResponseListenerHandler extends Handler {
    ResponseListener responseListener;
    public ResponseListenerHandler(ResponseListener responseListener){
        this.responseListener = responseListener;
    }
    @Override
    public void handleMessage(Message msg){
        responseListener.responseReceived(msg.getData().getString(AppConfig.RESPONSE_KEY));
    }
}
