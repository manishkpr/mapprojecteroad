package manishkpr.mapprojecteroad.framework;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import manishkpr.mapprojecteroad.R;
import manishkpr.mapprojecteroad.common.ConnectionDetector;
import manishkpr.mapprojecteroad.common.Prompt;
import manishkpr.mapprojecteroad.common.Utils;
import manishkpr.mapprojecteroad.config.AppConfig;
import manishkpr.mapprojecteroad.webservices.CommonWeb;


public class RequestTask extends AsyncTask<String,Void,String> {

    final int PROGRESS_START        =       1;
    final int PROGRESS_STOP         =       2;


    private final Handler responseListenerHandler;
    Activity activity;
    String urlMethod;



    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch (msg.what) {
                case PROGRESS_START:
                    counter = new ServiceCounter(AppConfig.DISCONNECT_TIMEOUT,1000);
                    counter.start();
                    break;
                case PROGRESS_STOP:
                    if(counter!=null)
                        counter.cancel();
                    break;

            }
        }

    };


    public RequestTask(Activity activity,Handler responseListenerHandler,String  urlMethod){
        this.responseListenerHandler      =  responseListenerHandler;
        this.urlMethod                    =  urlMethod;
        this.activity                     =  activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(!new ConnectionDetector(activity).isConnectingToInternet()){
            this.cancel(true);
            Prompt.showToast(activity, activity.getString(R.string.connection_error));
        }else{
            handler.sendEmptyMessage(PROGRESS_START);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        return new CommonWeb().getResponse(urlMethod);
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        handler.sendEmptyMessage(PROGRESS_STOP);
        responseListenerHandler.sendMessage(Utils.bundledMessage(responseListenerHandler, result));
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        Log.e("Task Canceled", "True");
        handler.sendEmptyMessage(PROGRESS_STOP);
    }
    ServiceCounter counter;
    public class ServiceCounter extends CountDownTimer {

        public ServiceCounter(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            handler.sendEmptyMessage(PROGRESS_STOP);
            RequestTask.this.cancel(true);
            Prompt.showToast(activity, activity.getString(R.string.connection_time_out)
            );
        }

        @Override
        public void onTick(long millisUntilFinished) {
            Log.e("Timer ", "" + (millisUntilFinished / 1000));
        }
    }
}
