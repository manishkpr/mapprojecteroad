package manishkpr.mapprojecteroad.framework;

public interface ResponseListener {
    void responseReceived(String response);
}
