package manishkpr.mapprojecteroad.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class DateTime {


    public static String getUTCTime(String timeZone){
        SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");
        f.setTimeZone(TimeZone.getTimeZone(timeZone));
        String utc = f.format(new Date());
        return utc;
    }
}
