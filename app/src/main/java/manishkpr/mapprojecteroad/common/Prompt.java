package manishkpr.mapprojecteroad.common;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

public class Prompt {
    /***
	* Class to display prompt using toast
	*/
    public static void showToast(final Activity activity,final String str){
    	activity.runOnUiThread(new Runnable() {
    	    @Override
			public void run() {
    	        Toast.makeText(activity, str, Toast.LENGTH_SHORT).show();
    	    }
    	});
    }
    public static void showToast(final Context context,final String str){
        Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
    }
}
