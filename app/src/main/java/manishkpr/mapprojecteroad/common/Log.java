package manishkpr.mapprojecteroad.common;


import manishkpr.mapprojecteroad.config.AppConfig;

public class Log {
     public static void e(String tag, String message){
    	 if(AppConfig.APP_DEBUG){
    		 android.util.Log.e(tag, message);
    	 }
     }
}
