package manishkpr.mapprojecteroad.common;

import android.app.Activity;
import android.app.ProgressDialog;

import manishkpr.mapprojecteroad.R;


public class PD {
	static ProgressDialog pd;
	public static void startProgress(Activity context){
		pd = new ProgressDialog(context);
		if(!pd.isShowing()){
			pd.setTitle(context.getString(R.string.please_wait));
			pd.setMessage(context.getString(R.string.loading));
			pd.setCancelable(true);
			pd.show();
		}else{
			pd.dismiss();
		}
	}
	public static void stopProgress(){
		if(pd.isShowing()){
		   pd.dismiss();
		}
	}
}
