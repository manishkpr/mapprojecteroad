package manishkpr.mapprojecteroad.common;

import org.json.JSONException;
import org.json.JSONObject;

public class GetJSONString {
    public static String getJsonString(String response,String key){
        String val="";
        if(response!=null){
            if(!response.equalsIgnoreCase("")){
                try {
                    val = new JSONObject(response).getString(key);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return val;
    }
}
