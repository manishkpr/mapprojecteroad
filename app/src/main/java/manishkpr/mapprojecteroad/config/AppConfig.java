package manishkpr.mapprojecteroad.config;


public class AppConfig {
    public static final Boolean APP_DEBUG         =       true;
    public static final long DISCONNECT_TIMEOUT   =       1*10*1000;

    public static String RESPONSE_KEY             =       "response_key";
    public static final String MAIN_URL			  = 	  "https://maps.googleapis.com/maps/api/timezone/json?";
}
