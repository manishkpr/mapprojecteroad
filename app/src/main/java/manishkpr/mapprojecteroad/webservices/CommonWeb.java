package manishkpr.mapprojecteroad.webservices;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;


public class CommonWeb {
    RestTemplate restTemplate;

    public CommonWeb() {
        restTemplate = new RestTemplate();
    }

    //#### Method To Get Information using GET METHOD
    public String getResponse(String paramUrl) {
        String response = "";
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        try {
            response = restTemplate.getForObject(paramUrl, String.class);
        } catch (ResourceAccessException e) {
        }
        return response;
    }
}