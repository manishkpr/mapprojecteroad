package manishkpr.mapprojecteroad;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.TimeZone;

import manishkpr.mapprojecteroad.adapters.MapInfoAdapter;
import manishkpr.mapprojecteroad.common.CurrentLocation;
import manishkpr.mapprojecteroad.common.DateTime;
import manishkpr.mapprojecteroad.common.GetJSONString;
import manishkpr.mapprojecteroad.common.Log;
import manishkpr.mapprojecteroad.config.AppConfig;
import manishkpr.mapprojecteroad.framework.RequestTask;
import manishkpr.mapprojecteroad.framework.ResponseListener;
import manishkpr.mapprojecteroad.framework.ResponseListenerHandler;

public class MainActivity extends AppCompatActivity {

    private CurrentLocation locationGps;
    private GoogleMap googleMap;
    private RequestTask requestTask;
    private MapInfoAdapter mapInfoAdapter;
    private Marker marker;
    private String timeZone = "";

    // latitude and longitude
    double latitude  = 0.0;
    double longitude = 0.0;

    double e_latitude  = -36.722195;
    double e_longitude = 174.706166;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    //## Init Map
    private void initMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            // check if map is created successfully or not
            timeZone = TimeZone.getDefault().getID();
            currentLocation();
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(), getString(R.string.sorry_unable_to_create_map), Toast.LENGTH_SHORT).show();
            }
        }
    }
    //## Get Time Zone By Latitude And Longitude
    void getTimeZone(){
        String url = AppConfig.MAIN_URL+"location="+latitude+","+longitude+"&timestamp="+System.currentTimeMillis()/1000+"&key="+getString(R.string.api_key);
        requestTask = new RequestTask(this,responseListenerHandler, url);
        requestTask.execute();
    }
    //## Once response will receive from web service will update bubble
    ResponseListenerHandler responseListenerHandler = new ResponseListenerHandler(new ResponseListener() {
        @Override
        public void responseReceived(String response) {
            timeZone = GetJSONString.getJsonString(response, "timeZoneId");
            notifyMapDataChanged();
        }
    });
    //## Method to Display Marker/Bubble
    void showMarker(){
        LatLng cLocation              = new LatLng(latitude, longitude);
        mapInfoAdapter = new MapInfoAdapter(this);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(cLocation).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        marker = googleMap.addMarker(new MarkerOptions().position(cLocation));
        marker.showInfoWindow();
        notifyMapDataChanged();
    }
    @Override
    protected void onResume() {
        super.onResume();
        initMap();
    }
    //## Method For Current location
    void currentLocation(){
        locationGps = new CurrentLocation(this);
        if(locationGps.canGetLocation()){
            double latitude = locationGps.getLatitude();
            double longitude = locationGps.getLongitude();
            if(latitude!=0 && longitude!=0){
                this.latitude=latitude;
                this.longitude=longitude;
                getTimeZone();
                showMarker();
            }

        }else{
            locationGps.showSettingsAlert();
        }

    }
    //## Method to update bubble information
    protected void notifyMapDataChanged() {
        Log.e("Time Zone ", timeZone);
        mapInfoAdapter.setLatitude("Latitude: " + String.valueOf(latitude));
        mapInfoAdapter.setLongitude("Longitude: " + String.valueOf(longitude));
        mapInfoAdapter.setLocalTime("Local Time: " + DateTime.getUTCTime(timeZone));
        mapInfoAdapter.setUtcTime("UTC: " + DateTime.getUTCTime("UTC"));
        mapInfoAdapter.setTimeZone("Time Zone: " + timeZone);//" \n"+CurrentLocation.getDurationMinute(latitude,longitude,e_latitude,e_longitude));
        googleMap.setInfoWindowAdapter(mapInfoAdapter);
        marker.showInfoWindow();
    }

}
